#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>
#include "raylib.h"

#define MAXLINE 1024

int playerJoined = false;
int opponentColor;
int opponentX;
int opponentY;

struct listen_thread_params
{
    int sock;
    char *buffer;
    struct sockaddr_in taraddr;
    socklen_t len;
};

void *handle_opponent_pos(char *buf)
{
    playerJoined = true;
    char *ptr = strtok(buf, ",");
    int i = 0;
    do
    {
        if (i == 0)
        {
            opponentColor = atoi(ptr);
        }
        else if (i == 1)
        {
            opponentX = atoi(ptr);
        }
        else if (i == 2)
        {
            opponentY = atoi(ptr);
        }
        i++;
        ptr = strtok(NULL, ",");
    } while (ptr != NULL);
    return 0;
}

void *listen_loop(void *params)
{
    struct listen_thread_params *thread_params = params;

    for (;;)
    {
        int n = recvfrom(thread_params->sock, (char *)thread_params->buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *)&thread_params->taraddr, &thread_params->len);
        thread_params->buffer[n] = '\0';

        printf("< %s\n", thread_params->buffer);

        handle_opponent_pos(thread_params->buffer);
    }
}

int main(int argc, char *argv[])
{

    int self_port = atoi(argv[1]);
    int target_port = atoi(argv[2]);
    int drop_percentage = atoi(argv[3]);

    pthread_t thread_id;

    char buffer[MAXLINE];
    char msg[MAXLINE] = "[connect]";

    struct sockaddr_in taraddr, selfaddr;
    memset(&selfaddr, 0, sizeof(selfaddr));
    memset(&taraddr, 0, sizeof(taraddr));

    selfaddr.sin_family = AF_INET;
    selfaddr.sin_addr.s_addr = INADDR_ANY;
    selfaddr.sin_port = htons(self_port);

    // todo: use dns name and resolve on dns instead
    const char *ip = "34.218.246.149";
    taraddr.sin_family = AF_INET;
    taraddr.sin_addr.s_addr = inet_addr(ip);
    taraddr.sin_port = htons(target_port);

    socklen_t len = sizeof(taraddr);

    // create a socket with IP, UDP
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("failed to create socket");
        exit(1);
    }

    if (bind(sock, (const struct sockaddr *)&selfaddr, sizeof(selfaddr)) < 0)
    {
        perror("failed to bind socket to address");
        exit(2);
    }

    sendto(sock, (const char *)&msg, strlen((const char *)&msg), 0, (const struct sockaddr *)&taraddr, len);

    struct listen_thread_params thread_params;
    thread_params.sock = sock;
    thread_params.buffer = buffer;
    thread_params.taraddr = taraddr;
    thread_params.len = len;
    pthread_create(&thread_id, NULL, listen_loop, &thread_params);

    const int screenWidth = 600;
    const int screenHeight = 400;

    InitWindow(screenWidth, screenHeight, "chop chop test");

    SetTargetFPS(60);

    int i = 0;
    int xspeed = 3;
    int yspeed = 3;
    int x = rand() % screenWidth;
    int y = rand() % screenHeight;

    while (!WindowShouldClose())
    {
        if (x <= 0)
        {
            xspeed = 3;
        }
        else if (x >= screenWidth)
        {
            xspeed = -3;
        }
        if (y <= 0)
        {
            yspeed = 3;
        }
        else if (y >= screenHeight)
        {
            yspeed = -3;
        }

        x += xspeed;
        y += yspeed;

        BeginDrawing();

        DrawCircle(x, y, 35, PURPLE);

        if (playerJoined == true)
        {
            DrawCircle(opponentX, opponentY, 35, ORANGE);
        }

        ClearBackground(RAYWHITE);

        EndDrawing();

        char xstr[99] = {0};
        sprintf(xstr, "%d", x);
        char ystr[99] = {0};
        sprintf(ystr, "%d", y);

        strcpy(msg, "1");
        strcat(msg, ",");
        strcat(msg, xstr);
        strcat(msg, ",");
        strcat(msg, ystr);

        if (rand() % 100 <= drop_percentage)
        {
            continue;
        }
        sendto(sock, (const char *)&msg, strlen((const char *)&msg), 0, (const struct sockaddr *)&taraddr, len);
    }

    CloseWindow();

    return 0;
}
