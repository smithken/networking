#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 3000
#define MAXLINE 256

// allows up to 12 concurrent clients
struct sockaddr_in clientList[12];
int clientListSize = 0;

char *conmsg = "[connect]";

void sendToClients(int sock, char *buffer, int len, int originport)
{
    printf("in send\n");
    for (int i = 0; i < clientListSize; i++)
    {
        if (originport == clientList[i].sin_port)
        {
            continue;
        }

        // printf("sending something i think to %d\n", clientList[i].sin_port);
        sendto(sock, (const char *)buffer, strlen(buffer), 0, (const struct sockaddr *)&clientList[i], len);
    }
}

int main()
{
    int sockfd;
    char buffer[MAXLINE];

    struct sockaddr_in servaddr, cliaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET; //IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    socklen_t len = sizeof(cliaddr);

    // create a socket with IP, UDP
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("failed to create socket");
        exit(1);
    }

    if (bind(sock, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("failed to bind socket to address");
        exit(2);
    }

    for (;;)
    {
        int n = recvfrom(sock, (char *)buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *)&cliaddr, &len);
        buffer[n] = '\0';

        printf("Message from client: %s\n", buffer);

        if (strcmp(conmsg, buffer) == 0)
        {
            clientList[clientListSize] = cliaddr;
            clientListSize++;
        }
        else
        {
            sendToClients(sock, buffer, len, cliaddr.sin_port);
        }
        // sendto(sock, (const char *)buffer, strlen(buffer), 0, (const struct sockaddr *)&cliaddr, len);
    }
}