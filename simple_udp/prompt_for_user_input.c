#include <stdio.h>

#define MAX_LEN 1024

int main(int argc, char *argv[])
{
    char a_word[MAX_LEN];

    for (;;)
    {
        printf("> ");
        scanf("%s", a_word);
        printf("< %s\n", a_word);
    }
}