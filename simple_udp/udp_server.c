#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 3000
#define MAXLINE 1024

int main()
{
    int sockfd;
    char buffer[MAXLINE];

    struct sockaddr_in servaddr, cliaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET; //IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    socklen_t len = sizeof(cliaddr);

    // create a socket with IP, UDP
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("failed to create socket");
        exit(1);
    }

    if (bind(sock, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("failed to bind socket to address");
        exit(2);
    }

    for (;;)
    {
        int n = recvfrom(sock, (char *)buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *)&cliaddr, &len);
        buffer[n] = '\0';

        printf("Message from client: %s\n", buffer);

        sendto(sock, (const char *)buffer, strlen(buffer), 0, (const struct sockaddr *)&cliaddr, len);
    }
}