#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

#define MAXLINE 1024

struct listen_thread_params
{
    int sock;
    char *buffer;
    struct sockaddr_in taraddr;
    socklen_t len;
};

void *listen_loop(void *params)
{
    const char *ack_msg = "[ack]\n";
    struct listen_thread_params *thread_params = params;

    for (;;)
    {
        int n = recvfrom(thread_params->sock, (char *)thread_params->buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *)&thread_params->taraddr, &thread_params->len);
        thread_params->buffer[n] = '\0';

        printf("< %s", thread_params->buffer);
        if (strncmp(thread_params->buffer, ack_msg, MAXLINE) != 0)
        {
            sendto(thread_params->sock, (const char *)ack_msg, strlen(ack_msg), 0, (const struct sockaddr *)&thread_params->taraddr, thread_params->len);
        }
    }
}

int main(int argc, char *argv[])
{
    int self_port = atoi(argv[1]);
    int target_port = atoi(argv[2]);

    pthread_t thread_id;

    char buffer[MAXLINE];
    char msg[MAXLINE];

    struct sockaddr_in taraddr, selfaddr;
    memset(&selfaddr, 0, sizeof(selfaddr));
    memset(&taraddr, 0, sizeof(taraddr));

    selfaddr.sin_family = AF_INET;
    selfaddr.sin_addr.s_addr = INADDR_ANY;
    selfaddr.sin_port = htons(self_port);

    taraddr.sin_family = AF_INET;
    taraddr.sin_addr.s_addr = INADDR_ANY;
    taraddr.sin_port = htons(target_port);

    socklen_t len = sizeof(taraddr);

    // create a socket with IP, UDP
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("failed to create socket");
        exit(1);
    }

    if (bind(sock, (const struct sockaddr *)&selfaddr, sizeof(selfaddr)) < 0)
    {
        perror("failed to bind socket to address");
        exit(2);
    }

    struct listen_thread_params thread_params;
    thread_params.sock = sock;
    thread_params.buffer = buffer;
    thread_params.taraddr = taraddr;
    thread_params.len = len;
    pthread_create(&thread_id, NULL, listen_loop, &thread_params);

    for (;;)
    {
        fgets(msg, MAXLINE, stdin);

        sendto(sock, (const char *)&msg, strlen((const char *)&msg), 0, (const struct sockaddr *)&taraddr, len);
    }
}